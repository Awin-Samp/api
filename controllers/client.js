'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const random = require('random');
const multer = require('multer');
// const redis = require('redis');
const nodemailer = require('nodemailer');
require('dotenv/config');
const Client_db = require('../models/clientdb');
const { builtinModules } = require('module');

const router = express.Router();
// const clientRed = redis.createClient();

//Set up middlewares
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: false}));

//Instantiate Client db
let Client = new Client_db();

//Image upload - multer config
const upload = multer({
    // dest: 'images',
    limits: {
    fileSize: 1000000,
    },
    fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(png|jpg|jpeg)$/)){
    cb(new Error('Please upload an image.'))
    }
    cb(undefined, true)
    }
    })




//------------------------------------------------


//--------------------------------------------USER REGISTRATION
router.post('/register', (req, res)=>{
    //GET ALL INPUTS
    var username = req.body.username;
    var email = req.body.email;
    var password = req.body.password;
    //DATE AND TIME
    const oldDate = new Date()
    var date = oldDate.toISOString().split('T')[0];
    var time  = oldDate.toLocaleTimeString(); 
    //ENCRYPT PASSWORD
    var hashedPassword = bcrypt.hashSync(String(password), 10);
    //NEW TOKEN GENERATED
    var token = random.int(1000,10000);
    
    //CHECK FOR EMPTY PARAMETERS
    if(!username || !email || !password){
        return res.status(404).json({
            status:false,
            message: 'Fill all fields'
        })
    }
    //  console.log(username, email, password,token, date, time, hashedPassword)
    Client.register_user( username, email, hashedPassword,token, date, time, (response)=>{
            if(response.status ==true){
                // console.log('Registered user is OTP Verified');
                res.status(201).json({
                    status:true,
                    message: 'New user created',
                    other: response.message,
                    token: token,  
                });
                return;
            }else{
                res.status(404).json({
                    status: false,
                    message: 'Unsuccessful registration',
                    other: response.message
                });
                return;
        }
    })
    
})


//--------------------------------------------USER LOGIN
// LOGIN ROUTE
router.post('/login', async (req, res)=>{
    var email = req.body.email;
    var password = req.body.password;

    //CHECK FOR EMPTY PARAMETERS
    if(!email || !password){
        return res.status(404).json({
            status:false,
            message: 'Fill all fields'
        })

    }


       Client.get_user(email, (response)=>{
        //    console.log("Client Password:", response  )
            if(response.status==true){
                // console.log("Client Password:", response.response.password)
                bcrypt.compare(password, response.response.password).then((result)=>{
                if(result ==true){
                    // console.log('bcrypt message', result)
                     //sending response 
                    return res.send({
                        status: true,
                        message: 'user login successful',
                        user: response.response
                    }) 
                }else{
                    res.status(404).send({
                        status: false, 
                        message: 'password incorrect!', 
                    })
                }
           }).catch((err)=>{ 
            //    throw err;
               res.status(500).send({
                status: false, 
                message: 'password error!',
                other:response.message, 
            })
            return;
           })
        }
        else{
            res.status(404).json({
                status: false,
                message:response.message,
                other: "email can't be empty!"
            }) 
        } 
    });
     
});


   

//--------------------------------------------USER PROFILE
router.get('/profile/:token', (req, res)=>{
    var token = req.params.token;

    // clientRed.get(token, (error, result)=>{
    //     if(error) throw error;
    //     if(result){
    //         res.status(201).json({
    //             status:true,
    //             message: 'Request successful',
    //             response: result,
    //         });
    //     }else{
             //GET PROFILE FROM DATABASE
                Client.get_user_by_token(token, (response)=>{
                    if(response.status ==true){
                        //CHACHED PROFILE
                        // clientRed.setex('token', 3600, JSON.stringify('response.response'));
                        
                        res.status(201).json({
                            status:true,
                            message: 'Request successful',
                            response: response.response,
                        });
                        return;
                    }else{
                        res.status(404).json({
                            status: false,
                            message: 'Request Unsuccessful ',
                            other: response.message
                        });
                        return;
                    }
                })
        // }
    // })
})

router.post('/profile_image', upload.single('upload'), async (req, res)=>{
    var user_id = req.body.user_id;
    //how to get image in nodejs request
    // console.log('user_id :>> ', user_id);
    try { 
        Client.upload_profile(req.file.buffer, user_id, (response)=>{
            if(response.status ==true){
                res.status(201).json({
                    status:true,
                    message: response.message,
                });
                return;
            }else{
                res.status(404).json({
                    status: false,
                    message: response.message
                });
                return;
            }
        }) 
        }
    catch (e){
        console.log('error hello', e)
        res.status(400).send(e)
        }
    }, (error, req, res, next) => {
        res.status(400).json({error: error.message})  
})


router.get('/profile_image/:user_id', (req,res)=>{
    var user_id = req.params.user_id;
    res.set('Content-Type', 'image/png')
    Client.get_user_image(user_id, (response)=>{
        // console.log('response :>> ', response);
        if(response.status ==true){
            res.status(200).json({response: response.response.image})
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})
//--------------------------------------------CHANGE PASSWORD
router.post('/change_password', (req,res)=>{
    var user_id = req.body.user_id;
    var password = req.body.password;

    //ENCRYPT PASSWORD
    var hashedPassword = bcrypt.hashSync(String(password), 10);
    Client.update_password(hashedPassword, user_id, (response)=>{
        // console.log('response :>> ', response);
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: 'Password changed successful',
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: 'Password change Unsuccessful ',
                other: response.message
            });
            return;
        }
    })
})


//--------------------------------------------USER RATING
router.post('/rate', (req, res)=>{
    var user_id = req.body.user_id;
    var business_id = req.body.business_id;
    var rating_count = req.body.rating_count;
    var comment = req.body.comment;
     //DATE AND TIME
     const oldDate = new Date()
     var date = oldDate.toISOString().split('T')[0];
     var time = oldDate.toLocaleTimeString(); 


     //SEND TO DATABASE
    //  console.log(user_id, business_id, rating_count, comment, date, time)
     Client.add_rating(user_id, business_id, rating_count, comment, date, time, (response)=>{
        console.log('response :>> ', response);
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message:'Business rated successfully',
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------FAVOURITES
router.post('/favourite', (req, res)=>{
    var user_id = req.body.user_id;
    var business_id = req.body.business_id;
     //DATE AND TIME
     const oldDate = new Date()
     var date = oldDate.toISOString().split('T')[0];
     var time  = oldDate.toLocaleTimeString(); 

     //SEND TO DATABASE 
     Client.add_favourite(user_id, business_id, date, time, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
     })
})


router.get('/favourite/:user_id', (req, res)=>{
    var user_id = req.params.user_id;
    //GET FROM DATABASE
    Client.get_favourite(user_id, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response:response.response
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

router.get('/unfavourite/:favourite_id', (req, res)=>{
    var favourite_id = req.params.favourite_id;
    //GET FROM DATABASE
    Client.unfavourite(favourite_id, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

// --------------------------------------------------------------------------------------------------------------------------------

//--------------------------------------------ADS BANNER
router.get('/ads', (req,res)=>{
    Client.get_ads((response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})


//--------------------------------------------POPULAR
router.get('/popular', (req,res)=>{
    Client.get_by_popular((response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})


//--------------------------------------------BEST RATING
router.get('/best_rating/', (req,res)=>{
    // var id = req.params.id;
    Client.get_business_by_rating( (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------BUSINESS PROFILE
router.get('/business_profile/:business_id', (req,res)=>{
    var business_id = req.params.business_id;

    Client.get_business_profile(business_id,(response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                avg_rating: response.avg_rating,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------RATINGS & REVIEWS
router.get('/business_reviews/:business_id', (req,res)=>{
    var business_id = req.params.business_id;

    Client.get_business_reviews(business_id,(response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                avg_rating: response.avg_rating,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------PRODUCT & SERVICES
router.get('/business_products/:id', (req,res)=>{
    var id = req.params.id;
    Client.get_business_products(id, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})
 


//--------------------------------------------LOCATIONS LIST

router.get('/locations', (req,res)=>{
    Client.get_locations((response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------CATEGORY LIST

router.get('/categories', (req,res)=>{
    Client.get_categories((response)=>{ 
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})


//--------------------------------------------BUSINESS PER CATEGORY
router.get('/category_group/:id', (req,res)=>{
    var id = req.params.id;
    Client.get_businesses_by_category(id, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

//--------------------------------------------SEARCH BY LOCATION AND PLACES
//SEARCH BY PLACES

router.post('/search_places/', (req,res)=>{
    // var id = req.params.id;
    var value = req.body.value;

    Client.get_by_search(value, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})


//SEARCH BY PLACES and LOCATION

router.post('/search_with_location/', (req,res)=>{
    // var id = req.params.id;
    var value = req.body.value;
    var location = req.body.location;

    Client.get_by_search_n_location(value,location, (response)=>{
        if(response.status ==true){
            res.status(201).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})



// PLACES NEARBY

router.post('/places_nearby/', (req,res)=>{
    // var id = req.params.id;
    var category_id = req.body.category_id;
    var location = req.body.location;

    Client.get_by_cat_n_location(category_id,location, (response)=>{
        if(response.status ==true){
            res.status(200).json({
                status:true,
                message: response.message,
                response: response.response,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})



router.post('/reset_password/', async (req,res)=>{
    var email = req.body.email; 
    // var user_id = req.body.user_id;

    //Emailing configuration
var transporter = nodemailer.createTransport({
    host: 'awinteck.com', 
    port: 465,
    secure: true,
    auth: {
        type: 'OAuth2',
        user: process.env.EMAIL, // generated ethereal user
        pass: process.env.PASSWORD, 
    }
  });

    Client.get_token(email, (response)=>{
        if(response.status ==true){
            var mailOptions = {
                from: 'no-reply@listed.com',
                to: email,
                subject: 'Listed Reset Password',
                html: '<h1>Listed Reset Password</h1> <p>You have initiated a password reset through the app</p> <div><h3> Reset Code : <a href="">'+ response.response.token +'</a></h3></div> <br> <br> <h4>Ignore this message if you did not initiate this.</h4> <br> <h5>Listed Corporation Ltd.</h5>'
              };
            
                transporter.sendMail(mailOptions, function(error, info){
                    if (error) {
                      console.log(error);
                      res.status(404).json({

                        status: false,
                        message: 'error',
                        response: response.response,
                    });
                    return;
                    } else {
                      console.log('Email sent: ' + info.response);
                        // console.log('response.response.token :>> ', response.response.token);
                        res.status(201).json({
                            status:true,
                            message: response.message,
                            response: response.response,
                        });
                        return;
                    }
                })
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})



// PLACES NEARBY

router.post('/send_message', (req,res)=>{
    var message = req.body.message;
    var phone = req.body.phone;
    var business_id = req.body.business_id;
    var user_id = req.body.user_id;

    console.log(message,phone,business_id,user_id)
    Client.send_message(user_id,business_id,phone,message, (response)=>{
        if(response.status ==true){
            res.status(200).json({
                status:true,
                message: response.message,
            });
            return;
        }else{
            res.status(404).json({
                status: false,
                message: response.message
            });
            return;
        }
    })
})

module.exports = router;
