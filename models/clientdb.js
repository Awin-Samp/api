const mysql = require('mysql')
const database = require('./database')

class client_db {
    constructor(){
        global.db = database;
    }

     // LOGIN SECTION ---------------------------------------------------------------------------
  get_user(email, callback) {
    let query = "SELECT * FROM users WHERE email=?";
 
      try {
        db.query(query,[email], (err, response) => {
          if (err) {
            //   throw err;
            return callback({ status: false, message: "error here" });
          }
          if (response.length == 0) {
            return callback({
              status: false,
              message: "User not registered",
            });
          } else {
            console.log("user response", response[0]);
            return callback({
              status: true,
              message: "user exists",
              response: response[0],
            });
          }
        });
      } catch (err) {
        return callback({
          status: false,
          message: "failed user login (email not in database)",
        });
      } 
  }


 // USERS DATABASE SECTION--------------------------------------------------------------
  // registration for users to userdb
  register_user(username,email,password,token,date,time, callback) {
    let query = "INSERT INTO `users` (`username`, `email`, `password`,`token`,`date`,`time`) VALUES (?)";
    var values = [username,email,password,token,date,time ];

    // check if phone number is not already registered
    this.get_user(email, (result) => {
      if (result.status != true) {
        //means email is not registered, so can add new user
        try {
          db.query(query, [values], (err, response) => {
            if (err) {
                // throw err;
              console.log("MY ERROR", err);
               callback({ status: false, message: "error here" });
            }
            console.log("Registered new user", response);
            if (response.length == 0) {
               callback({ status: false, message: "No data available" });
            }else{

                 callback({
              status: true,
              message: "user registered successfully",
              response,
            });
            }
            
          });
        } catch (err) {
          console.log("Myerr", err);
           callback({
            status: false,
            message: "failed user registration",
          });
        }
      } else {
        console.log("get user email doesn't exist", "");
         callback({
          status: false,
          message: "Email already registered",
        });
      }
    });
  }


       // GET USER BY TOKEN SECTION ---------------------------------------------------------------------------
       get_user_by_token(token, callback) {
        let query = "SELECT * FROM users WHERE token=?";
     
          try {
            db.query(query,[token], (err, response) => {
              if (err) {
                //   throw err;
                return callback({ status: false, message: "error here" });
              }
              if (response.length == 0) {
                return callback({
                  status: false,
                  message: "User not registered",
                });
              } else {
                console.log("user response", response[0]);
                return callback({
                  status: true,
                  message: "user exists",
                  response: response[0],
                });
              }
            });
          } catch (err) {
            return callback({
              status: false,
              message: "failed user login (email not in database)",
            });
          } 
      }



       // GET USER BY ID SECTION ---------------------------------------------------------------------------
       get_user_by_id(id, callback) {
        let query = "SELECT username,image FROM users WHERE id=?";
     
          try {
            db.query(query,[id], (err, response) => {
              if (err) {
                //   throw err;
                return callback({ status: false, message: "error here" });
              }
              if (response.length == 0) {
                return callback({
                  status: false,
                  message: "User not registered",
                });
              } else {
                // console.log("user response", response[0]);
                return callback({
                  status: true,
                  message: "user exists",
                  response: response[0],
                });
              }
            });
          } catch (err) {
            return callback({
              status: false,
              message: "failed user login (email not in database)",
            });
          } 
      }



      upload_profile(buffer, user_id, callback){
          let query = "UPDATE users SET image=? WHERE id=?"
          try {
              db.query(query, [buffer, user_id], (error, response)=>{
                if(error) throw error;
                else if (response.changedRows == 0) {
                    return callback({
                      status: false,
                      message: "Nothing happened",
                    });
                  }
                  else{
                      return callback({
                          status:true,
                          message: 'Image upload successful'
                      })
                  }
              })
          } catch (error) {
            return callback({
                status: false,
                message: "failed upload image",
              });
          }
      }

           // USER PROFILE IMAGE SECTION ---------------------------------------------------------------------------
  get_user_image(user_id, callback) {
    let query = "SELECT image FROM users WHERE id=?";
 
      try {
        db.query(query,[user_id], (err, response) => {
          if (err) {
            //   throw err;
            return callback({ status: false, message: "error here" });
          } 
          if (response.length == 0) {
            return callback({
              status: false,
              message: "User image not available",
            });
          } else {
            console.log("user image response", response[0]);
            return callback({
              status: true,
              message: "user image exists",
              response: response[0],
            });
          }
        });
      } catch (err) {
        return callback({
          status: false,
          message: "failed user login (email not in database)",
        });
      } 
  }



  //CHANGE PASSWORD--------------------------------------

  update_password(password, user_id, callback){
    let query = "UPDATE users SET password=? WHERE id=?"
    try {
        db.query(query, [password, user_id], (error, response)=>{
          if(error) throw error;
          else if (response.changedRows == 0) {
              return callback({
                status: false,
                message: "Password is the same",
              });
            }
            else{
                return callback({
                    status:true,
                    message: 'Password updated successful'
                })
            }
        })
    } catch (error) {
      return callback({
          status: false,
          message: "failed to update password",
        });
    }
}


    //CATEGORY LIST----------------------------------------------
    get_categories(callback) {
        let query = "SELECT * FROM categories";
    
        try {
            db.query(query, (err, response) => {
            if (err) {
                //   throw err;
                return callback({ status: false, message: "error here" });
            } 
            if (response.length == 0) {
                return callback({
                status: false,
                message: "not available",
                });
            } else {
                // console.log("category response", response);
                return callback({
                status: true,
                message: "Category request successful",
                response: response,
                });
            }
            });
        } catch (err) {
            return callback({
            status: false,
            message: "failed request",
            });
        } 
  }



    //LOCATIONS LIST----------------------------------------------
    get_locations(callback) {
        let query = "SELECT * FROM locations";
    
        try {
            db.query(query, (err, response) => {
            if (err) {
                //   throw err;
                return callback({ status: false, message: "error here" });
            } 
            if (response.length == 0) {
                return callback({
                status: false,
                message: "not available",
                });
            } else {
                // console.log("location response", response);
                return callback({
                status: true,
                message: "Location request successful",
                response: response,
                });
            }
            });
        } catch (err) {
            return callback({
            status: false,
            message: "failed request",
            });
        } 
  }


    //FAVOURITE LIST----------------------------------------------
  add_favourite(user_id, business_id, date, time, callback){
    let query = "INSERT INTO favourites (user_id, business_id, date, time) VALUES (?)"
    var values = [user_id,business_id,date,time]
    try {
        db.query(query, [values], (error, response)=>{
            if (error) {
                  throw error;
                return callback({ status: false, message: "error here" });
            } 
            console.log("Favourite", response);
            if (response.length == 0) {
               callback({ status: false, message: "No data available" });
            }else{
                 callback({
                    status: true,
                    message: "Favourite added successfully", 
                    });
            }
        })
    } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
    }
  }


  //Unfavourite --------------------
  unfavourite(favourite_id, callback){
    let query = "DELETE FROM `favourites` WHERE id=?" 
    
    try {
        db.query(query, [favourite_id], (error, response)=>{
            if (error) {
                //   throw error;
                return callback({ status: false, message: "error here" });
            } 
            console.log("Favourite", response);
            if (response.affectedRows == 0) {
               callback({ status: false, message: "No data available" });
            }else{
                 callback({
                    status: true,
                    message: "Favourite removed successfully", 
                    });
            }
        })
    } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
    }
  }




  get_favourite(user_id, callback){
    let query = "SELECT * FROM favourites WHERE user_id=?"
    
    try {
        var favs = [];
        db.query(query, [user_id], (error, response)=>{
            if (error) {
                //   throw error;
                return callback({ status: false, message: "error here" });
            } 
            // console.log("Favourite", response);
            if (response.length == 0) {
               return callback({ status: false, message: "No data available" });
            }else{
                var business_ids = response;
                var business_length = response.length; 
                var count = 0;
                business_ids.forEach((id)=>{ 
                    // console.log('count, business_length :>> ', count, business_length);
                    // console.log("loop in", id['business_id']);
                    this.get_business_by_id(id['business_id'], (result)=>{
                        // console.log("Favourite in", response);
                        if(result.status==true){ 

                            favs.push({
                              fav_id:response[count]['id'],
                              avg_rating:result.avg_rating,
                              response: result.response
                          }) 
                          count++;

                            //return data after finish looping
                            if(0 == --business_length){
                                // console.log("Favourite 2", favs);
                                  callback({
                                    status: true,
                                    message: "Favourite added successfully", 
                                    response: favs
                                    });
                            }

                        }else{
                            return callback({
                                status: false,
                                message: "request unsuccessful", 
                                });
                        }
                    })
                })
            }
        })
    } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
    }
  }


//--------------------------------------------BUSINESS PROFILE
get_business_profile(id, callback){
    let query = "SELECT `id`, `name`, `location`, `address`, `description`, `category_id`, `category`, `image1`, `image2`, `image3`, `image4`, `image5`, `gps`, `website`, `email`, `tel`, `instagram`, `twitter`, `facebook` FROM business WHERE id=?"
    try {
        db.query(query, [id], (error, response)=>{
          if (error) {
              //   throw error;
              return callback({ status: false, message: "error here" });
          } 
          // console.log("Data", response);
          if (response.length == 0) {
             return callback({ status: false, message: "No data available" });
          }else{
            this.get_business_rating(id, (result)=>{
                if(result.status==true){
                   return callback({
                       status: true,
                       message: "Business info requested successfully", 
                       avg_rating:result.response,
                       response: response[0]
                       });
                }
                else{
                   return callback({
                       status: false,
                       message: "Business aveg rating failed request", 
                       });
                }
            })
              }

        })
    } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
}
  //Get business info
  get_business_by_id(id, callback){
      let query = "SELECT id, name, `category_id`, category, description, image1 FROM business WHERE id=?"
      try {
          db.query(query, [id], (error, response)=>{
            if (error) {
                //   throw error;
                return callback({ status: false, message: "error here" });
            } 
            // console.log("Data", response);
            if (response.length == 0) {
               return callback({ status: false, message: "No data available" });
            }else{
                 this.get_business_rating(id, (result)=>{
                     if(result.status==true){
                        return callback({
                            status: true,
                            message: "Business info requested successfully", 
                            avg_rating:result.response,
                            response: response[0]
                            });
                     }
                     else{
                        return callback({
                            status: false,
                            message: "Business aveg rating failed request", 
                            });
                     }
                 })
                }

          })
      } catch (error) {
            return callback({
                status: false,
                message: "failed request",
                });
        }
  }



  //RATING----------------------------------------------------------
  add_rating(user_id, business_id, rating_count, comment, date, time, callback){
      let query = "INSERT INTO ratings (user_id, business_id, rating_count, comment, date, time) VALUES (?)"
      var values = [user_id, business_id, rating_count, comment, date, time];
      try {
        db.query(query, [values], (error, response)=>{
          if (error) {
                // throw error;
              return callback({ status: false, message: "error here" });
          } 
          // console.log("Data", response);
          if (response.length == 0) {
             return callback({ status: false, message: "No data available" });
          }else{
               return callback({
                  status: true,
                  message: "Ratings added successfully"
                  });
              }

        })
    } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
  }


//--------------------------------------------RATINGS & REVIEWS
get_business_reviews(business_id, callback){
    let query = "SELECT * FROM ratings WHERE business_id=? ORDER BY id DESC"
    try {
        var reviews = [];
        
        db.query(query, [business_id], (error, response)=>{
            if (error) {
                // throw error;
              return callback({ status: false, message: "error here" });
                } 
                // console.log("Average", response);
                if (response.length == 0) {
                    return callback({ status: false, message: "No data available" });
                }else{
                    var excellent = 0;
                    var very_goood = 0;
                    var average = 0;
                    var poor = 0;
                    var bad = 0;
                    var len = response.length;
                    var data ={};
                    response.forEach((val)=>{
                        if(parseInt(val['rating_count']) == 5){ excellent++}
                        else if(parseInt(val['rating_count']) == 4){ very_goood++}
                        else if(parseInt(val['rating_count']) == 3){ average++}
                        else if(parseInt(val['rating_count']) == 2){ poor++}
                        else{ bad++}
                            this.get_user_by_id(val['user_id'], (result)=>{
                                if(result.status==true){ 
                                    data = {
                                        comment: val['comment'],
                                        rating_count: val['rating_count'],
                                        date: val['date'],
                                        user_id: val['user_id'],
                                        username: result.response['username'],
                                        image: result.response['image']
                                    }
                                    // console.log("data 2",data);
                                    reviews.push(data)
                                    //return data after finish looping
                                    if(0 == --len){
                                        // console.log("Favourite 2");
                                        return callback({
                                            status: true,
                                            message: "Business ratings & reviews requested successfully", 
                                            response: {
                                                excellent,
                                                very_goood,
                                                average,
                                                poor,
                                                bad,
                                                reviews: reviews.slice(0,10)
                                            }
                                        });
                                    } 
                                }
                            })  
                    })
                }
          })
    } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
    }
}


  //Get average rating for business
  get_business_rating(business_id, callback){
      let query = "SELECT AVG(rating_count) AS avg_rate FROM ratings WHERE business_id=?"
      try {
          db.query(query, [business_id], (error, response)=>{
            if (error) {
                // throw error;
              return callback({ status: false, message: "error here" });
          } 
        //   console.log("Average", response);
          if (response.length == 0) {
             return callback({ status: false, message: "No data available" });
          }else{
               return callback({
                  status: true,
                  message: "Business Average Ratings requested successfully",
                  response: Math.round((response[0]['avg_rate']) * 10) / 10
                  });
              }
          })
      } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
      }
  }


  //--------------------------------------------PRODUCT & SERVICES
  get_business_products(business_id, callback){
    let query = "SELECT * FROM products WHERE business_id=? ORDER BY id DESC"
    try {
        db.query(query, [business_id], (error, response)=>{
          if (error) {
              // throw error;
            return callback({ status: false, message: "error here" });
        }
        // console.log("Average", response);
        if (response.length == 0) {
           return callback({ status: false, message: "No data available" });
        }else{
             return callback({
                status: true,
                message: "Business products requested successfully",
                response: response
                });
            }
        })
    } catch (error) {
      return callback({
          status: false,
          message: "failed request",
          });
    }
}





//--------------------------------------------BUSINESS PER CATEGORY
get_businesses_by_category(category_id, callback){
    let query = "SELECT `id`, `name`, `location`, `description`, `category_id`, `category`, `image1` FROM business WHERE category_id=?"
    try {
        var businesses =[];
        db.query(query, [category_id], (error, response)=>{
          if (error) {
              //   throw error;
              return callback({ status: false, message: "error here" });
          } 
          console.log("Data", response);
          if (response.length == 0) {
             return callback({ status: false, message: "No data available" });
          }else{
            var business_length = response.length;
            response.forEach((val)=>{ 
                // console.log('count, business_length :>> ', count, business_length);
                // console.log("loop in", id['business_id']);
                this.get_business_rating(val['id'], (result)=>{
                    if(result.status==true){
                        var data = {
                            id: val['id'], 
                            name: val['name'],
                            location: val['location'], 
                            address: val['address'], 
                            description: val['idescriptiond'],
                            category_id: val['category_id'], 
                            category: val['category'], 
                            rating: result.response,
                            image1: val['image1'],
                            image2: val['image2'], 
                            image3: val['image3'], 
                            image4: val['image4'], 
                            image5: val['image5'], 
                            gps: val['gps'], 
                            website: val['website'],
                            email: val['email'], 
                            tel: val['tel'], 
                            instagram: val['instagram'],
                            twitter: val['twitter'], 
                            facebook: val['facebook']
                        }
                        businesses.push(data);
                    //return data after finish looping
                            if(0 == --business_length){
                                console.log("Favourite 2",businesses);
                                callback({
                                    status: true,
                                    message: "Business by category requested successfully", 
                                    response: businesses
                                    });
                            }
                    }
                    else{
                    return callback({
                        status: false,
                        message: "failed request", 
                        });
                    }
                })
            })
              }
        })
    } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
}


//--------------------------------------------ADS BANNER
get_ads(callback) {
    let query = "SELECT * FROM ads ORDER BY score DESC LIMIT 10";

    try {
        db.query(query, (err, response) => {
        if (err) {
              // throw err;
            return callback({ status: false, message: "error here" });
        } 
        if (response.length == 0) {
            return callback({
            status: false,
            message: "not available",
            });
        } else {
            // console.log("ads response", response);
            return callback({
            status: true,
            message: "Ads request successful",
            response: response,
            });
        }
        });
    } catch (err) {
        return callback({
        status: false,
        message: "failed request",
        });
    } 
}




//--------------------------------------------POPULAR
get_by_popular(callback) {
  let query = "SELECT id,name,image1,category_id FROM business ORDER BY popular DESC LIMIT 10";

  try {
          db.query(query, (err, response)=>{
              if (err) {
                  //   throw err;
                  return callback({ status: false, message: "error here" });
              } 
              if (response.length == 0) {
                  return callback({
                  status: false,
                  message: "not available",
                  });
              } else {
                  // console.log("business id response", response);
                  var business_ids = response
                  var len = response.length
                  var arr = []
                  var data = {}
                  //get avg rating for each business
                  business_ids.forEach((single)=>{
                      this.get_business_rating(single['id'], (response)=>{
                          if(response.status==true){
                              // console.log('AVG rating :>> ', {single:single['id'], 'avg': response.response});
                              data = {
                                  business_id:single['id'],
                                  category_id: single['category_id'],
                                  'avg': response.response,
                                  image: single['image1'],
                                  name: single['name']
                              }
                              arr.push(data)
                              if(0 == --len){
                                  arr.sort((a, b) => a.avg > b.avg ? -1 : 1); //arrange avg_rating of business in descending order
                                  return callback({
                                      status: true,
                                      message: " request successful",
                                      response: arr,
                                      });
                              }
                          }
                      })
                  })
              }  
          })
      } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
}


//--------------------------------------------BEST RATING
  get_business_by_rating(callback){
      let query = 'SELECT id, image1, name, category_id FROM business ORDER BY popular DESC LIMIT 10';
      try {
          db.query(query, (err, response)=>{
              if (err) {
                  //   throw err;
                  return callback({ status: false, message: "error here" });
              } 
              if (response.length == 0) {
                  return callback({
                  status: false,
                  message: "not available",
                  });
              } else {
                  // console.log("business id response", response);
                  var business_ids = response
                  var len = response.length
                  var arr = []
                  var data = {}
                  //get avg rating for each business
                  business_ids.forEach((single)=>{
                      this.get_business_rating(single['id'], (response)=>{
                          if(response.status==true){
                              // console.log('AVG rating :>> ', {single:single['id'], 'avg': response.response});
                              data = {
                                  business_id:single['id'],
                                  category_id: single['category_id'],
                                  'avg': response.response,
                                  image: single['image1'],
                                  name: single['name']
                              }
                              arr.push(data)
                              if(0 == --len){
                                  arr.sort((a, b) => a.avg > b.avg ? -1 : 1); //arrange avg_rating of business in descending order
                                  return callback({
                                      status: true,
                                      message: " request successful",
                                      response: arr,
                                      });
                              }
                          }
                      })
                  })
              }  
          })
      } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
  }


    //--------------------------------------------SEARCH
get_by_search(value, callback) {
    // let query = "SELECT * FROM business WHERE name LIKE '%li%';"
    let query = "SELECT id,name,image1,category_id,location FROM business WHERE name LIKE '%"+value+"%' OR category LIKE '%"+value+"%' OR description LIKE '%"+value+"%';"
      try {
        db.query(query, (err, response)=>{
            if (err) {
                //   throw err;
                return callback({ status: false, message: "error here" });
            } 
            if (response.length == 0) {
                return callback({
                status: false,
                message: "not available",
                });
            } else {
                // console.log("business id response", response);
                var business_ids = response
                var len = response.length
                var arr = []
                var data = {}
                //get avg rating for each business
                business_ids.forEach((single)=>{
                    this.get_business_rating(single['id'], (response)=>{
                        if(response.status==true){
                            // console.log('AVG rating :>> ', {single:single['id'], 'avg': response.response});
                            data = {
                                business_id:single['id'],
                                category_id: single['category_id'],
                                'avg': response.response,
                                image: single['image1'],
                                name: single['name'],
                                location: single['location']
                            }
                            arr.push(data)
                            if(0 == --len){
                                arr.sort((a, b) => a.avg > b.avg ? -1 : 1); //arrange avg_rating of business in descending order
                                return callback({
                                    status: true,
                                    message: " request successful",
                                    response: arr,
                                    });
                            }
                        }
                    })
                })
            }  
        })
    } catch (error) {
        return callback({
            status: false,
            message: "failed request",
            });
    }
}

get_by_search_n_location(value1, value2, callback) {
    let query = "SELECT id,name,image1,category_id,location FROM business WHERE address LIKE '%"+value2+"%' AND name LIKE '%"+value1+"%' OR category LIKE '%"+value1+"%' OR description LIKE '%"+value1+"%';"

    try {
      db.query(query, (err, response)=>{
          if (err) {
              //   throw err;
              return callback({ status: false, message: "error here" });
          } 
          if (response.length == 0) {
              return callback({
              status: false,
              message: "not available",
              });
          } else {
              // console.log("business id response", response);
              var business_ids = response
              var len = response.length
              var arr = []
              var data = {}
              //get avg rating for each business
              business_ids.forEach((single)=>{
                  this.get_business_rating(single['id'], (response)=>{
                      if(response.status==true){
                          // console.log('AVG rating :>> ', {single:single['id'], 'avg': response.response});
                          data = {
                              business_id:single['id'],
                              category_id: single['category_id'],
                              'avg': response.response,
                              image: single['image1'],
                              name: single['name'],
                              location: single['location']
                          }
                          arr.push(data)
                          if(0 == --len){
                              arr.sort((a, b) => a.avg > b.avg ? -1 : 1); //arrange avg_rating of business in descending order
                              return callback({
                                  status: true,
                                  message: " request successful",
                                  response: arr,
                                  });
                          }
                      }
                  })
              })
          }  
      })
  } catch (error) {
      return callback({
          status: false,
          message: "failed request",
          });
  }
}

// PLACES NEARBY BUSINESS USING LOCATION AND CATEGORY
get_by_cat_n_location(value1, value2, callback) {
  let query = "SELECT id,name,image1,category_id FROM business WHERE address LIKE '%"+value2+"%' AND category_id = '"+value1+"';"

  try {
          db.query(query, (err, response)=>{
              if (err) {
                  //   throw err;
                  return callback({ status: false, message: "error here" });
              } 
              if (response.length == 0) {
                  return callback({
                  status: false,
                  message: "not available",
                  });
              } else {
                  // console.log("business id response", response);
                  var business_ids = response
                  var len = response.length
                  var arr = []
                  var data = {}
                  //get avg rating for each business
                  business_ids.forEach((single)=>{
                      this.get_business_rating(single['id'], (response)=>{
                          if(response.status==true){
                              // console.log('AVG rating :>> ', {single:single['id'], 'avg': response.response});
                              data = {
                                  business_id:single['id'],
                                  category_id: single['category_id'],
                                  'avg': response.response,
                                  image: single['image1'],
                                  name: single['name'],
                                  location: single['location']
                              }
                              arr.push(data)
                              if(0 == --len){
                                  arr.sort((a, b) => a.avg > b.avg ? -1 : 1); //arrange avg_rating of business in descending order
                                  return callback({
                                      status: true,
                                      message: " request successful",
                                      response: arr,
                                      });
                              }
                          }
                      })
                  })
              }  
          })
      } catch (error) {
          return callback({
              status: false,
              message: "failed request",
              });
      }
}



//GET USER TOKEN FOR EMAILING 
get_token(email, callback) {
    let query = "SELECT user_id, token FROM users WHERE email=?"

    try {
        db.query(query, [email], (err, response) => {
        if (err) {
              // throw err;
            return callback({ status: false, message: "error here" });
        } 
        if (response.length == 0) {
            return callback({
            status: false,
            message: "Email not registered",
            });
        } else {
            // console.log("ads response", response);
            return callback({
            status: true,
            message: "user token request successful",
            response: response[0],
            });
        }
        });
    } catch (err) { 
        return callback({
        status: false,
        message: "failed request",
        });
    } 
}


//SEND MESSAGES
send_message(user_id,business_id,phone,message,callback){
  let query = "INSERT INTO `messages` (`user_id`, `business_id`, `phone`,`message`) VALUES (?)";
  var values = [user_id,business_id,phone,message];

      //means email is not registered, so can add new user
      try {
        db.query(query, [values], (err, response) => {
          if (err) {
              // throw err;
            // console.log("MY ERROR", err);
             callback({ status: false, message: "System error here" });
          }
          // console.log("Message added", response);
          if (response.length == 0) {
             callback({ status: false, message: "No data available" });
          }else{
               callback({
            status: true,
            message: "Message added successfully",
            response,
          });
          }
          
        });
      } catch (err) {
        // console.log("Myerr", err);
         callback({
          status: false,
          message: "failed add message",
        });
      }
  }

}
module.exports = client_db