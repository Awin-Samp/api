'use strict';

const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt'); 
require('dotenv/config');

const client = require('./controllers/client')

const app = express();

app.use(cors({
    origin: '*'
}));
var PORT = process.env.PORT || 2000;
app.use('/api/client/', client)
app.get('/', (req, res)=>{
    res.send('hello world')
})



app.listen(PORT, console.log('server on 2000'))
